from django.contrib import admin
from django.urls import path, include
from libro.views import Inicio
from django.contrib.auth import login,logout    


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('libro.urls')),
    path('inicio/', Inicio.as_view(), name ="index"),
    path('login', login , { 'template_name':'login.html'} , name ="login" )


]