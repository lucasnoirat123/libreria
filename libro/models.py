from django.db import models

# Create your models here.


class Autor(models.Model):
    id           = models.AutoField(primary_key=True)
    apellido     = models.CharField(max_length=40, blank=False, null=False)
    nombre       = models.CharField(max_length=30, blank=False, null=False)
    nacionalidad = models.CharField(max_length=30, blank=False, null=False)
    descripcion  = models.TextField(blank=True)
    estado       = models.BooleanField('estado' , default=True)
    class meta:
        verbose_name        = 'autor'
        verbose_name_plural = 'autores'
        ordering            = ['nombre']

    def __str__(self):
        return self.nombre



class libro(models.Model):
    id                 =  models.AutoField(primary_key=True)
    titulo             =  models.CharField(max_length=30, blank=False, null=False)
    fecha_publicacion  =  models.DateField('fecha de publicacion', null=True, blank=False)
    id_autor           =  models.ManyToManyField(Autor)
    

    class Meta:
        verbose_name        = 'Libro'
        verbose_name_plural = 'Libros'

    def __str__(self):
        return self.titulo

    
    