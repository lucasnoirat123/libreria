from django.http import request
from django.shortcuts import redirect, render
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse_lazy

from django.views.generic import TemplateView 
from django.views.generic import DeleteView
from django.views.generic import CreateView
from django.views.generic import UpdateView
from django.views.generic import ListView

from .forms import AutorForm ,LibroForm
from .models import Autor, libro
# Create your views here.

#me devuelve al home

def crearAutor(request):
    if request.method =='POST':
        autor_form = AutorForm(request.POST)
        if autor_form.is_valid():
            autor_form.save()
            return redirect('crearAutor')

        else: 
            autor_form = AutorForm()        
       
        print(autor_form)
        return render(request, 'crearAutor.html', {'autor_form':autor_form}) 

def crearAutor(request):
    
    if request.method == 'POST':
        nom = request.POST.get('nombre')
        ape = request.POST.get('apellido')
        nacion = request.POST.get('nacionalidad')
        descp = request.POST.get('descripcion')
        autor = Autor(nombre= nom , apellido = ape, nacionalidad = nacion , descripcion = descp)
        autor.save()
        
        return redirect('listar_autor' )       
      
    return render(request,'crearAutor.html'  )




#mmostrar listado de autores
def listarAutor(request):

    autores= Autor.objects.filter(estado=True)
    return render(request, 'listarAutor.html', {'autores':autores})

# Editar el autor
def editarAutor(request,id):
    autor_form= None
    error =None
    try:
     autor= Autor.objects.get(id=id) 
     if request.method== 'GET':                                 #devolviendo los valores actuales del objeto requerido
         autor_form = AutorForm(instance= autor)
     else:
         autor_form = AutorForm(request.POST, instance=autor)    # Pasando nuevos valores 
         if autor_form.is_valid():
             autor_form.save()
             return redirect('listar_autor')           #si es todo correcto muestra el listado actualizado
    except ObjectDoesNotExist as e:                              #excepcion si no se puedee hacer el cambio
        error= e

    
    return render(request , 'crearAutor.html' , {'error': error, 'autor':autor})  




def eliminarAutor(request,id):
    autor= Autor.objects.get(id= id)
    if request.method =='POST':                 
        autor.estado = False   #eliminacion logica de un dato * - si quiero borrar fisicamente --> autor.delete()
        autor.save()
        return redirect('listar_autor')
    
    return render(request, 'eliminarAutor.html', {'autor':autor})   



def listarLibro(request):

    return render(request, 'listarLibro.html')



# VISTAS BASADAS EN CLASES ------------------------------------------ FUNCIONES ARRIBA (ESTAN PARA HACER FACHA O MIRARLAS MAS TARDE XD)

class Inicio(TemplateView):
    template_name = 'index.html'

class ListadoAutor(ListView):
    model = Autor
    template_name = 'listarAutor.html'
    context_object_name = 'autores'
    queryset = Autor.objects.filter(estado = True)

class Actualizado(UpdateView):
    model = Autor
    context_object_name = 'autor'
    fields = ['nombre', 'apellido', 'nacionalidad', 'descripcion']
    template_name = 'crearAutor.html'
    success_url = reverse_lazy('listar_autor')

class CreadoAutor(CreateView):
    model = Autor
    fields =  ['nombre', 'apellido', 'nacionalidad', 'descripcion']
    template_name = 'crearAutor.html'
    success_url = reverse_lazy('listar_autor')

class Eliminado(DeleteView):
    model= Autor

    def post(self,request,pk, *args, **kwargs):
        object = Autor.objects.get(id= pk)
        object.estado = False
        object.save()
        return redirect('listar_autor')



# CRUD DE LIBROS

class ListadoLibro(ListView):
    model = libro
    template_name = 'ListarLibro.html'
    context_object_name = 'libros'


class EliminadoLibro(DeleteView):
    model= Autor

    def post(self,request,pk, *args, **kwargs):
        object = libro.objects.get(id= pk)
        object.estado = False
        object.save()
        return redirect('listar_libro')

class CrearLibro(CreateView):
    model = libro 
    form_class = LibroForm   
   
    template_name = 'crearLibro.html'
    context_object_name = 'libros'
    success_url = reverse_lazy('listar_libro')

    