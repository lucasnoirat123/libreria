from django.urls import path, include
from . import views 
from django.conf import settings
from django.conf.urls.static import static
from .views import Actualizado, CreadoAutor, CrearLibro, Eliminado, EliminadoLibro, ListadoAutor, ListadoLibro



urlpatterns = [


path('crearAutor', CreadoAutor.as_view() , name="crear_autor"),
path('listarAutor', ListadoAutor.as_view(), name="listar_autor"),
path('editarAutor/<int:pk>', Actualizado.as_view(), name= "editar_autor"),
path('eliminarAutor/<int:pk>', Eliminado.as_view(), name="eliminar_autor"),


path('crearLibro', CrearLibro.as_view(), name ="crear_libro"),
path('listarLibro', ListadoLibro.as_view(), name ="listar_libro"),
path('eliminarLibro/<int:pk>', EliminadoLibro.as_view(), name="eliminar_libro"),

]   

urlpatterns+=static(settings.MEDIA_URL , document_root=settings.MEDIA_ROOT)