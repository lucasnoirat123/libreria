from django import forms
from django.db.models import fields
from django.db.models.fields.related import create_many_to_many_intermediary_model
from django.forms import widgets
from .models import Autor, libro




class AutorForm(forms.ModelForm):
    class Meta:
        model = Autor
        fields = [
            'nombre', 'apellido', 'nacionalidad', 'descripcion'
        ]




class LibroForm(forms.ModelForm):

    class Meta: 
        model = libro
        fields = [
            'titulo' , 'fecha_publicacion' , 'id_autor'
        ]


        widgets  = {
            'id_autor':  forms.SelectMultiple(
                attrs= {
                        'class':'form_control'
                }
            ),

            'fecha_publicacion': forms.SelectDateWidget(
              
            )

        


        }